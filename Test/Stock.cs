﻿using System;
namespace Test
{
        public partial class Stock
        {
            public int? Id { get; set; }
            public string? Productcode { get; set; }
            public double? StockQuantity { get; set; }
    }
}

