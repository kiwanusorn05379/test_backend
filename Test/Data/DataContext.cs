﻿using Microsoft.EntityFrameworkCore;
using Test;

namespace EF6SQLiteTutorial.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
          
        }
        public DbSet<MasterProduct> MasterProducts => Set<MasterProduct>();
        public DbSet<Stock> Stocks => Set<Stock>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterProduct>(entity =>
            {
                entity.ToTable("MasterProduct");

                entity.Property(e => e.Id);
                entity.Property(e => e.Productcode);
                entity.Property(e => e.Productname);
                entity.Property(e => e.Priceunit);


            });
            modelBuilder.Entity<Stock>(entity =>
            {
                entity.ToTable("Stock");

                entity.Property(e => e.Id);
                entity.Property(e => e.Productcode);
                entity.Property(e => e.StockQuantity);


            });
        }
    }
}
