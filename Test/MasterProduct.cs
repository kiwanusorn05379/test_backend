﻿using System;
using System;
namespace Test
{
    public partial class MasterProduct
    {
        public int? Id { get; set; }
        public string? Productcode { get; set; }
        public string? Productname { get; set; }
        public string? Priceunit { get; set; }
    }
}

