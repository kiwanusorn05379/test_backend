﻿using System;
using Newtonsoft.Json;

namespace Test
{
    public class ResponseMessage
    {
        #region Properties
        public bool success { get; set; } = true;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string code { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string message { get; set; }
        #endregion

        public ResponseMessage() { }
        public ResponseMessage(bool success)
        {
            this.success = success;
        }

        #region Method
        public void SetFault(Exception ex)
        {
            success = false;
            code = ex.HResult.ToString();
            message = ex.Message;
        }

        public void SetFault(string message)
        {
            success = false;
            this.message = message;
        }

        public void SetFault(string code, string message)
        {
            success = false;
            this.code = code;
            this.message = message;
        }
        #endregion
    }
}

