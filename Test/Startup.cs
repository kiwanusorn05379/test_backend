﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy("AllowOrigin", builder =>
            {
                builder.WithOrigins("http://localhost:3001") // Replace with the actual origin of your React application
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            });
        });

        // Other service configurations...
    }

    public void Configure(IApplicationBuilder app)
    {
        app.UseCors("AllowOrigin");

        // Other app configurations...

        app.UseRouting();

        // Other middleware configurations...

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
