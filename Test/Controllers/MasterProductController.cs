﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EF6SQLiteTutorial.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Test;
using System.Collections;
using System.Diagnostics.Metrics;
using System.Web.Http.Results;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    public class MasterProductController : Controller
    {
        private readonly DataContext _context;

        public MasterProductController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<MasterProductAndStock>> GetProduct([FromQuery] MasterProductAndStock input)
        {

            var query = from mp in _context.MasterProducts
                        join sp in _context.Stocks on mp.Productcode equals sp.Productcode
                        group new { mp, sp } by new { mp.Productcode, mp.Productname, mp.Priceunit } into g
                        select new
                        {
                            g.Key.Productcode,
                            g.Key.Productname,
                            g.Key.Priceunit,
                            StockQuantity = g.Sum(x => x.sp.StockQuantity)
                        };

            var result = query.ToList();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<bool>> AddStock([FromBody] List<Stock> stock)
        {
            try
            {
                if (stock.Count > 0)
                {
                    foreach (var item in stock)
                    {
                        item.StockQuantity = -item.StockQuantity;
                        _context.Stocks.Add(new Stock()
                        {
                            Productcode = item.Productcode,
                            StockQuantity =  item.StockQuantity
                        });
                    }
                    await _context.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
