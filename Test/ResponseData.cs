﻿using System;
namespace Test
{
    public class ResponseData<T> : ResponseMessage
    {
        public ResponseData() { }
        public ResponseData(bool success) : base(success) { }
        public T data { get; set; }


    }
}

