﻿using System;
namespace Test
{
    public partial class MasterProductAndStock
    {
        public int? Id { get; set; }
        public string? Productcode { get; set; }
        public string? Productname { get; set; }
        public string? Priceunit { get; set; }
        public double? StockQuantity { get; set; }
    }
}

